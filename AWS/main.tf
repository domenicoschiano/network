
provider "aws" {
  region     = "eu-west-1"
  access_key = ""
  secret_key = ""
  token      = ""
}


module "VPC-SUBNETs" {
    source              = "./MODULES/NETWORK/VPC-SUBNETs"

    vpc_subnet_cidr     = "192.168.1.0/24"
    subnets_count       = "4" 
    nbit                = "2"
    subname             = ["FRONTEND", "BACKEND", "PUBLIC", "DMZ",]
}

module "SECURITY" {
    source              = "./MODULES/NETWORK/SECURITY"

    vpc_termod          = module.VPC-SUBNETs.vpc-termod
}

module "GATEWAYS" {
    source              = "./MODULES/NETWORK/GATEWAYS"

    vpc_termod          = module.VPC-SUBNETs.vpc-termod 
    subnets_termod      = module.VPC-SUBNETs.subnets 
    subnets_dmz         = module.VPC-SUBNETs.subnet-dmz
    bgp_customer_gw     = "65000"
    customer_vpn_ip     = "84.220.214.221"
}    

module "ROUTING" {
    source              = "./MODULES/NETWORK/ROUTING"

    vpc_termod          = module.VPC-SUBNETs.vpc-termod 
    subnets_termod      = module.VPC-SUBNETs.subnets 
    nat_gateway         = module.GATEWAYS.nat-gateway
    transit_gateway     = module.GATEWAYS.transit-gateway
    subnets_dmz         = module.VPC-SUBNETs.subnet-dmz
}    