output "private_subnet_ids" {
  value = module.VPC-SUBNETs.subnet-ids
}

output "internet-gateway" {
  value = module.GATEWAYS.internet-gateway
}

output "public-ip" {
  value = module.GATEWAYS.public-ip
}

output "nat-gateway" {
  value = module.GATEWAYS.nat-gateway
}

output "transit-gateway" {
  value = module.GATEWAYS.transit-gateway
}

output "public-route-table" {
  value = module.ROUTING.public-route-table
}