variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "subnets_termod" {
    description = "subnets id"
    type = string
}

variable "nat_gateway" {
    description = "nat gateway id"
    type = string
}

variable "transit_gateway" {
    description = "transit gateway id"
    type = string
}

variable "subnets_dmz" {
    description = "subnet DMZ id"
    type = string
}