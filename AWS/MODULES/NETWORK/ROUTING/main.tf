resource "aws_route_table" "public-termod" {
  vpc_id = var.vpc_termod

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = var.nat_gateway
  }

  tags = {
    Name = "public-termod"
  }
}

resource "aws_route_table_association" "rt-association-termod" {
  subnet_id      = var.subnets_termod
  route_table_id = aws_route_table.public-termod.id
}

resource "aws_route_table" "internal-termod" {
  vpc_id = var.vpc_termod

  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway
  }

  tags = {
    Name = "internal-termod"
  }
}

resource "aws_route_table_association" "rt-association-internal-termod" {
  subnet_id      = var.subnets_dmz
  route_table_id = aws_route_table.internal-termod.id
}