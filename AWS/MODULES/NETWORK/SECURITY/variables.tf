variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "pub_icc" {
    description = "ICC Pub Subnet"
    type = string
    default = "193.194.138.132/32"
}

variable "ingress_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [443, 22, 3389]
}