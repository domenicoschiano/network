resource "aws_security_group" "sg-termod" {
  name        = "global"
  description = "Ingress for global security group"
  vpc_id      = var.vpc_termod

  dynamic "ingress" {
    iterator = port
    for_each = var.ingress_ports
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = [var.pub_icc]
    }
  }
}
