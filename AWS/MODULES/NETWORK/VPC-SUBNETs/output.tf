output "vpc-termod" {
  value = aws_vpc.vpc-termod.id
}

output "subnets" {
  value = aws_subnet.subnets-termod[0].id
}

output "subnet-dmz" {
  value = aws_subnet.subnets-termod[3].id
}

output "subnet-ids" {
  value = aws_subnet.subnets-termod.*.id
}