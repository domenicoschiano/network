output "internet-gateway" {
  value = aws_internet_gateway.intgw-termod.id
}

output "public-ip" {
  value = aws_eip.public.public_ip
}

output "nat-gateway" {
  value = aws_nat_gateway.natgw-termod.id
}

output "transit-gateway" {
  value = aws_ec2_transit_gateway.tgw-termod.id
}
