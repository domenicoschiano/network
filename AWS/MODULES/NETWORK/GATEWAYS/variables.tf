variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "subnets_termod" {
    description = "subnets id"
    type = string
}

variable "subnets_dmz" {
    description = "subnets id"
    type = string
}

variable "bgp_customer_gw" {
    description = "customer BGP"
    type = string
}

variable "customer_vpn_ip" {
    description = "customer VPN IP"
    type = string
}