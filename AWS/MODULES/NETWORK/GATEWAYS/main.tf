resource "aws_internet_gateway" "intgw-termod" {
  vpc_id = var.vpc_termod

  tags = {
    Name = "intgw-termod"
  }
}

resource "aws_eip" "public" {
  vpc   = true

  tags = {
    Name = "eip-termod"
    }
}

resource "aws_nat_gateway" "natgw-termod" {
  allocation_id = aws_eip.public.id
  subnet_id     = var.subnets_termod

  tags = {
    Name = "natgw-termod"
  }
}

resource "aws_ec2_transit_gateway" "tgw-termod" {
  amazon_side_asn = 64532 

  tags = {
    Name = "tgw-termod"
  }
}

resource "aws_ec2_transit_gateway_vpc_attachment" "attach-termod" {
  subnet_ids         = [var.subnets_dmz]
  transit_gateway_id = aws_ec2_transit_gateway.tgw-termod.id
  vpc_id             = var.vpc_termod

 tags = {
    Name = "attach-termod"
 }
}

resource "aws_vpn_gateway" "vpn-gw-termod" {
  vpc_id = var.vpc_termod

  tags = {
    Name = "vpn-gw-termod"
  }
}

resource "aws_customer_gateway" "customer-gw-termod" {
  bgp_asn    = var.bgp_customer_gw
  ip_address = var.customer_vpn_ip
  type       = "ipsec.1"

  tags = {
    Name = "vpn-customer-gw-termod"
  }
}

resource "aws_vpn_connection" "vpn-vonnection-termod" {
  customer_gateway_id = aws_customer_gateway.customer-gw-termod.id
  transit_gateway_id  = aws_ec2_transit_gateway.tgw-termod.id
  type                = aws_customer_gateway.customer-gw-termod.type
}